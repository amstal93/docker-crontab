FROM alpine
LABEL author="Glen de Koning"

COPY crontab /var/spool/cron/crontabs/root
COPY entrypoint.sh /entrypoint.sh

RUN which crond && \
    chmod 755 /entrypoint.sh && \
    apk add --no-cache bash curl && \
    rm -rf /var/cache/apk/* && \
    rm -rf /etc/periodic

ENTRYPOINT ["/entrypoint.sh"]
CMD ["crond", "-f", "-l", "2"]